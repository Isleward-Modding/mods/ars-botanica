# Ars Botanica
"The Art of Botany" - new foods, drinks, ingredients, processing methods, crafting recipes, gathering methods, and more.

## License
Ars Botanica is licensed under the MIT License.
See LICENSE.txt for details.

This software includes third party open source software components and assets: Foo, Bar, and Whatsit (hey maybe i should put the actual credit? see credit.txt, it has them).
Each of these software components and corresponding assets have their own license.
Please see ./CREDIT.txt for their licenses.

## Short-Term TODO
* More "end product" foods (not processing/intermediates)

## Roadmap/Plan
* More fruits/vegetables (berry garden? fruit trees?)
** Tomato, onion (high priority). Garlic? 
* Greenhouse for growing a certain plant from one existing plant, over time
** Storage over time? How will this be implemented?
* Treetapping? Maple trees --> maple syrup
* 'Hungry' prophecy/hunger bar mechanic
* Custom fishing mechanics? More fish types, minigame?
* Generalize multi-map imposing (load multiple maps segments at once, configure which maps to paste on to).
** Not 100% needed because the only added area currently is the farmhand hut
* Dynamic sandwiches
* 'Festival'/cooking competition events? Cook the most variety in a time
* Chat command to get an item (especially crafting materials) w/ AssetAtlas sprite

## Dependencies
Asset Atlas by notme
Better Recipes by notme

## Core Changes
Nothing needs to be changed right now.
Developed on latest master (as of around Feb 14, 2019).

## Attributions
https://henrysoftware.itch.io/pixel-item, $7
Creative Commons Zero v1.0 Universal, MIT License
By Henry Software

A bunch of other art assets.
Credits are above (license section) or in CREDIT.txt.
