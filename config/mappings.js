module.exports = function (mappings) {
	[
		// mock
		'isleward/herbs',

		// tiles
		'gardens',

		// base items
		'grain',
		'spice',
		'fruitveg',
		'fish',
		'fluids',
		'misc/harvestfestival-crops',

		// crafting items
		'baking',
		'soup',

		// misc
		'misc',
		// all of coterie craft, under ab.items.cc.____
		'cc',
		// all of bird's foods
		'bf'
	].forEach(function (mapFile) {
		this.require('config/mappings/' + mapFile, true).bind(this)(mappings);
	}, this);

	// Might not be needed
	return mappings;
};
