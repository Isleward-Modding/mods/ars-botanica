module.exports = function (mappings) {
	[
		{ id: 'baking.wheat', position: [247, 0], sheet: 'ars-botanica.cc.items' },
		{ id: 'baking.flour', position: [9, 1], sheet: 'ars-botanica.harvest-festival.items' },
		{ id: 'baking.dough', position: [2, 1], sheet: 'ars-botanica.cuisine.items' },
		{ id: 'baking.bread', position: [26, 0], sheet: 'ars-botanica.henrysoftware.items' }

		// { id: '', position: [#, #] },
	].forEach(function (map) {
		map.id = 'ars-botanica.items.' + map.id;
		mappings.push(map);
	});
};
