module.exports = function (mappings) {
	let maps = [
		{ id: 'acacia_boat', sheet: 'ars-botanica.cc.items', position: [0, 0] },
		{ id: 'acacia_door', sheet: 'ars-botanica.cc.items', position: [1, 0] },
		{ id: 'apple', sheet: 'ars-botanica.cc.items', position: [2, 0] },
		{ id: 'armor_stand', sheet: 'ars-botanica.cc.items', position: [3, 0] },
		{ id: 'arrow', sheet: 'ars-botanica.cc.items', position: [4, 0] },
		{ id: 'baked_potato', sheet: 'ars-botanica.cc.items', position: [5, 0] },
		{ id: 'banner_base', sheet: 'ars-botanica.cc.items', position: [6, 0] },
		{ id: 'banner_overlay', sheet: 'ars-botanica.cc.items', position: [7, 0] },
		{ id: 'barrier', sheet: 'ars-botanica.cc.items', position: [8, 0] },
		{ id: 'bed', sheet: 'ars-botanica.cc.items', position: [9, 0] },
		{ id: 'beef', sheet: 'ars-botanica.cc.items', position: [10, 0] },
		{ id: 'beetroot', sheet: 'ars-botanica.cc.items', position: [11, 0] },
		{ id: 'beetroot_seeds', sheet: 'ars-botanica.cc.items', position: [12, 0] },
		{ id: 'beetroot_soup', sheet: 'ars-botanica.cc.items', position: [13, 0] },
		{ id: 'birch_boat', sheet: 'ars-botanica.cc.items', position: [14, 0] },
		{ id: 'birch_door', sheet: 'ars-botanica.cc.items', position: [15, 0] },
		{ id: 'blaze_powder', sheet: 'ars-botanica.cc.items', position: [16, 0] },
		{ id: 'blaze_rod', sheet: 'ars-botanica.cc.items', position: [17, 0] },
		{ id: 'boat', sheet: 'ars-botanica.cc.items', position: [18, 0] },
		{ id: 'bone', sheet: 'ars-botanica.cc.items', position: [19, 0] },
		{ id: 'bone_meal', sheet: 'ars-botanica.cc.items', position: [20, 0] },
		{ id: 'book', sheet: 'ars-botanica.cc.items', position: [21, 0] },
		{ id: 'bow', sheet: 'ars-botanica.cc.items', position: [22, 0] },
		{ id: 'bow_pulling_0', sheet: 'ars-botanica.cc.items', position: [23, 0] },
		{ id: 'bow_pulling_1', sheet: 'ars-botanica.cc.items', position: [24, 0] },
		{ id: 'bow_pulling_2', sheet: 'ars-botanica.cc.items', position: [25, 0] },
		{ id: 'bowl', sheet: 'ars-botanica.cc.items', position: [26, 0] },
		{ id: 'bread', sheet: 'ars-botanica.cc.items', position: [27, 0] },
		{ id: 'brewing_stand', sheet: 'ars-botanica.cc.items', position: [28, 0] },
		{ id: 'brick', sheet: 'ars-botanica.cc.items', position: [29, 0] },
		{ id: 'broken_elytra', sheet: 'ars-botanica.cc.items', position: [30, 0] },
		{ id: 'bucket', sheet: 'ars-botanica.cc.items', position: [31, 0] },
		{ id: 'cactus_green', sheet: 'ars-botanica.cc.items', position: [32, 0] },
		{ id: 'cake', sheet: 'ars-botanica.cc.items', position: [33, 0] },
		{ id: 'carrot', sheet: 'ars-botanica.cc.items', position: [34, 0] },
		{ id: 'carrot_on_a_stick', sheet: 'ars-botanica.cc.items', position: [35, 0] },
		{ id: 'cauldron', sheet: 'ars-botanica.cc.items', position: [36, 0] },
		{ id: 'chainmail_boots', sheet: 'ars-botanica.cc.items', position: [37, 0] },
		{ id: 'chainmail_chestplate', sheet: 'ars-botanica.cc.items', position: [38, 0] },
		{ id: 'chainmail_helmet', sheet: 'ars-botanica.cc.items', position: [39, 0] },
		{ id: 'chainmail_leggings', sheet: 'ars-botanica.cc.items', position: [40, 0] },
		{ id: 'charcoal', sheet: 'ars-botanica.cc.items', position: [41, 0] },
		{ id: 'chest_minecart', sheet: 'ars-botanica.cc.items', position: [42, 0] },
		{ id: 'chicken', sheet: 'ars-botanica.cc.items', position: [43, 0] },
		{ id: 'chorus_fruit', sheet: 'ars-botanica.cc.items', position: [44, 0] },
		{ id: 'clay_ball', sheet: 'ars-botanica.cc.items', position: [45, 0] },
		{ id: 'clock', sheet: 'ars-botanica.cc.items', position: [46, 0] },
		{ id: 'coal', sheet: 'ars-botanica.cc.items', position: [47, 0] },
		{ id: 'cocoa_beans', sheet: 'ars-botanica.cc.items', position: [48, 0] },
		{ id: 'cod', sheet: 'ars-botanica.cc.items', position: [49, 0] },
		{ id: 'cod_bucket', sheet: 'ars-botanica.cc.items', position: [50, 0] },
		{ id: 'command_block_minecart', sheet: 'ars-botanica.cc.items', position: [51, 0] },
		{ id: 'comparator', sheet: 'ars-botanica.cc.items', position: [52, 0] },
		{ id: 'compass', sheet: 'ars-botanica.cc.items', position: [53, 0] },
		{ id: 'cooked_beef', sheet: 'ars-botanica.cc.items', position: [54, 0] },
		{ id: 'cooked_chicken', sheet: 'ars-botanica.cc.items', position: [55, 0] },
		{ id: 'cooked_cod', sheet: 'ars-botanica.cc.items', position: [56, 0] },
		{ id: 'cooked_mutton', sheet: 'ars-botanica.cc.items', position: [57, 0] },
		{ id: 'cooked_porkchop', sheet: 'ars-botanica.cc.items', position: [58, 0] },
		{ id: 'cooked_rabbit', sheet: 'ars-botanica.cc.items', position: [59, 0] },
		{ id: 'cooked_salmon', sheet: 'ars-botanica.cc.items', position: [60, 0] },
		{ id: 'cookie', sheet: 'ars-botanica.cc.items', position: [61, 0] },
		{ id: 'cyan_dye', sheet: 'ars-botanica.cc.items', position: [62, 0] },
		{ id: 'dandelion_yellow', sheet: 'ars-botanica.cc.items', position: [63, 0] },
		{ id: 'dark_oak_boat', sheet: 'ars-botanica.cc.items', position: [64, 0] },
		{ id: 'dark_oak_door', sheet: 'ars-botanica.cc.items', position: [65, 0] },
		{ id: 'diamond', sheet: 'ars-botanica.cc.items', position: [66, 0] },
		{ id: 'diamond_axe', sheet: 'ars-botanica.cc.items', position: [67, 0] },
		{ id: 'diamond_boots', sheet: 'ars-botanica.cc.items', position: [68, 0] },
		{ id: 'diamond_chestplate', sheet: 'ars-botanica.cc.items', position: [69, 0] },
		{ id: 'diamond_helment', sheet: 'ars-botanica.cc.items', position: [70, 0] },
		{ id: 'diamond_hoe', sheet: 'ars-botanica.cc.items', position: [71, 0] },
		{ id: 'diamond_horse_armor', sheet: 'ars-botanica.cc.items', position: [72, 0] },
		{ id: 'diamond_leggings', sheet: 'ars-botanica.cc.items', position: [73, 0] },
		{ id: 'diamond_pickaxe', sheet: 'ars-botanica.cc.items', position: [74, 0] },
		{ id: 'diamond_shovel', sheet: 'ars-botanica.cc.items', position: [75, 0] },
		{ id: 'diamond_sword', sheet: 'ars-botanica.cc.items', position: [76, 0] },
		{ id: 'dragon_breath', sheet: 'ars-botanica.cc.items', position: [77, 0] },
		{ id: 'egg', sheet: 'ars-botanica.cc.items', position: [78, 0] },
		{ id: 'elytra', sheet: 'ars-botanica.cc.items', position: [79, 0] },
		{ id: 'emerald', sheet: 'ars-botanica.cc.items', position: [80, 0] },
		{ id: 'empty_armor_slot_boots', sheet: 'ars-botanica.cc.items', position: [81, 0] },
		{ id: 'empty_armor_slot_chestplate', sheet: 'ars-botanica.cc.items', position: [82, 0] },
		{ id: 'empty_armor_slot_helmet', sheet: 'ars-botanica.cc.items', position: [83, 0] },
		{ id: 'empty_armor_slot_leggings', sheet: 'ars-botanica.cc.items', position: [84, 0] },
		{ id: 'empty_armor_slot_shield', sheet: 'ars-botanica.cc.items', position: [85, 0] },
		{ id: 'enchanted_book', sheet: 'ars-botanica.cc.items', position: [86, 0] },
		{ id: 'ender_eye', sheet: 'ars-botanica.cc.items', position: [87, 0] },
		{ id: 'experience_bottle', sheet: 'ars-botanica.cc.items', position: [88, 0] },
		{ id: 'feather', sheet: 'ars-botanica.cc.items', position: [89, 0] },
		{ id: 'fermented_spider_eye', sheet: 'ars-botanica.cc.items', position: [90, 0] },
		{ id: 'filled_map_markings', sheet: 'ars-botanica.cc.items', position: [91, 0] },
		{ id: 'filled_map', sheet: 'ars-botanica.cc.items', position: [92, 0] },
		{ id: 'fire_charge', sheet: 'ars-botanica.cc.items', position: [93, 0] },
		{ id: 'firework_rocket', sheet: 'ars-botanica.cc.items', position: [94, 0] },
		{ id: 'firework_star_overlay', sheet: 'ars-botanica.cc.items', position: [95, 0] },
		{ id: 'firework_star', sheet: 'ars-botanica.cc.items', position: [96, 0] },
		{ id: 'fish_cod_cooked', sheet: 'ars-botanica.cc.items', position: [97, 0] },
		{ id: 'fishing_rod_cast', sheet: 'ars-botanica.cc.items', position: [98, 0] },
		{ id: 'fishing_rod', sheet: 'ars-botanica.cc.items', position: [99, 0] },
		{ id: 'flint', sheet: 'ars-botanica.cc.items', position: [100, 0] },
		{ id: 'flint_and_steel', sheet: 'ars-botanica.cc.items', position: [101, 0] },
		{ id: 'flower_pot', sheet: 'ars-botanica.cc.items', position: [102, 0] },
		{ id: 'furnace_minecart', sheet: 'ars-botanica.cc.items', position: [103, 0] },
		{ id: 'ghast_tear', sheet: 'ars-botanica.cc.items', position: [104, 0] },
		{ id: 'glass_bottle', sheet: 'ars-botanica.cc.items', position: [105, 0] },
		{ id: 'glistering_melon_slice', sheet: 'ars-botanica.cc.items', position: [106, 0] },
		{ id: 'glowstone_dust', sheet: 'ars-botanica.cc.items', position: [107, 0] },
		{ id: 'gold_ingot', sheet: 'ars-botanica.cc.items', position: [108, 0] },
		{ id: 'gold_nugget', sheet: 'ars-botanica.cc.items', position: [109, 0] },
		{ id: 'golden_apple', sheet: 'ars-botanica.cc.items', position: [110, 0] },
		{ id: 'golden_axe', sheet: 'ars-botanica.cc.items', position: [111, 0] },
		{ id: 'golden_boots', sheet: 'ars-botanica.cc.items', position: [112, 0] },
		{ id: 'golden_carrot', sheet: 'ars-botanica.cc.items', position: [113, 0] },
		{ id: 'golden_chestplate', sheet: 'ars-botanica.cc.items', position: [114, 0] },
		{ id: 'golden_helmet', sheet: 'ars-botanica.cc.items', position: [115, 0] },
		{ id: 'golden_hoe', sheet: 'ars-botanica.cc.items', position: [116, 0] },
		{ id: 'golden_horse_armor', sheet: 'ars-botanica.cc.items', position: [117, 0] },
		{ id: 'golden_leggings', sheet: 'ars-botanica.cc.items', position: [118, 0] },
		{ id: 'golden_pickaxe', sheet: 'ars-botanica.cc.items', position: [119, 0] },
		{ id: 'golden_shovel', sheet: 'ars-botanica.cc.items', position: [120, 0] },
		{ id: 'golden_sword', sheet: 'ars-botanica.cc.items', position: [121, 0] },
		{ id: 'gray_dye', sheet: 'ars-botanica.cc.items', position: [122, 0] },
		{ id: 'gunpowder', sheet: 'ars-botanica.cc.items', position: [123, 0] },
		{ id: 'hopper', sheet: 'ars-botanica.cc.items', position: [124, 0] },
		{ id: 'hopper_minecart', sheet: 'ars-botanica.cc.items', position: [125, 0] },
		{ id: 'ink_sac', sheet: 'ars-botanica.cc.items', position: [126, 0] },
		{ id: 'iron_axe', sheet: 'ars-botanica.cc.items', position: [127, 0] },
		{ id: 'iron_boots', sheet: 'ars-botanica.cc.items', position: [128, 0] },
		{ id: 'iron_chestplate', sheet: 'ars-botanica.cc.items', position: [129, 0] },
		{ id: 'iron_door', sheet: 'ars-botanica.cc.items', position: [130, 0] },
		{ id: 'iron_helmet', sheet: 'ars-botanica.cc.items', position: [131, 0] },
		{ id: 'iron_hoe', sheet: 'ars-botanica.cc.items', position: [132, 0] },
		{ id: 'iron_horse_armor', sheet: 'ars-botanica.cc.items', position: [133, 0] },
		{ id: 'iron_ingot', sheet: 'ars-botanica.cc.items', position: [134, 0] },
		{ id: 'iron_leggings', sheet: 'ars-botanica.cc.items', position: [135, 0] },
		{ id: 'iron_nugget', sheet: 'ars-botanica.cc.items', position: [136, 0] },
		{ id: 'iron_pickaxe', sheet: 'ars-botanica.cc.items', position: [137, 0] },
		{ id: 'iron_shovel', sheet: 'ars-botanica.cc.items', position: [138, 0] },
		{ id: 'iron_sword', sheet: 'ars-botanica.cc.items', position: [139, 0] },
		{ id: 'item_frame', sheet: 'ars-botanica.cc.items', position: [140, 0] },
		{ id: 'jungle_boat', sheet: 'ars-botanica.cc.items', position: [141, 0] },
		{ id: 'jungle_door', sheet: 'ars-botanica.cc.items', position: [142, 0] },
		{ id: 'knowledge_book', sheet: 'ars-botanica.cc.items', position: [143, 0] },
		{ id: 'lapis_lazuli', sheet: 'ars-botanica.cc.items', position: [144, 0] },
		{ id: 'lava_bucket', sheet: 'ars-botanica.cc.items', position: [145, 0] },
		{ id: 'lead', sheet: 'ars-botanica.cc.items', position: [146, 0] },
		{ id: 'leather', sheet: 'ars-botanica.cc.items', position: [147, 0] },
		{ id: 'leather_boots_overlay', sheet: 'ars-botanica.cc.items', position: [148, 0] },
		{ id: 'leather_boots', sheet: 'ars-botanica.cc.items', position: [149, 0] },
		{ id: 'leather_chestplate_overlay', sheet: 'ars-botanica.cc.items', position: [150, 0] },
		{ id: 'leather_chestplate', sheet: 'ars-botanica.cc.items', position: [151, 0] },
		{ id: 'leather_helmet_overlay', sheet: 'ars-botanica.cc.items', position: [152, 0] },
		{ id: 'leather_helmet', sheet: 'ars-botanica.cc.items', position: [153, 0] },
		{ id: 'leather_leggings_overlay', sheet: 'ars-botanica.cc.items', position: [154, 0] },
		{ id: 'leather_leggings', sheet: 'ars-botanica.cc.items', position: [155, 0] },
		{ id: 'light_blue_dye', sheet: 'ars-botanica.cc.items', position: [156, 0] },
		{ id: 'light_gray_dye', sheet: 'ars-botanica.cc.items', position: [157, 0] },
		{ id: 'lime_dye', sheet: 'ars-botanica.cc.items', position: [158, 0] },
		{ id: 'lingering_potion', sheet: 'ars-botanica.cc.items', position: [159, 0] },
		{ id: 'magenta_dye', sheet: 'ars-botanica.cc.items', position: [160, 0] },
		{ id: 'magma_cream', sheet: 'ars-botanica.cc.items', position: [161, 0] },
		{ id: 'map', sheet: 'ars-botanica.cc.items', position: [162, 0] },
		{ id: 'melon_seeds', sheet: 'ars-botanica.cc.items', position: [163, 0] },
		{ id: 'melon_slice', sheet: 'ars-botanica.cc.items', position: [164, 0] },
		{ id: 'milk_bucket', sheet: 'ars-botanica.cc.items', position: [165, 0] },
		{ id: 'minecart', sheet: 'ars-botanica.cc.items', position: [166, 0] },
		{ id: 'mushroom_stew', sheet: 'ars-botanica.cc.items', position: [167, 0] },
		{ id: 'music_disc_11', sheet: 'ars-botanica.cc.items', position: [168, 0] },
		{ id: 'music_disc_13', sheet: 'ars-botanica.cc.items', position: [169, 0] },
		{ id: 'music_disc_blocks', sheet: 'ars-botanica.cc.items', position: [170, 0] },
		{ id: 'music_disc_cat', sheet: 'ars-botanica.cc.items', position: [171, 0] },
		{ id: 'music_disc_chirp', sheet: 'ars-botanica.cc.items', position: [172, 0] },
		{ id: 'music_disc_far', sheet: 'ars-botanica.cc.items', position: [173, 0] },
		{ id: 'music_disc_mall', sheet: 'ars-botanica.cc.items', position: [174, 0] },
		{ id: 'music_disc_mellohi', sheet: 'ars-botanica.cc.items', position: [175, 0] },
		{ id: 'music_disc_stal', sheet: 'ars-botanica.cc.items', position: [176, 0] },
		{ id: 'music_disc_strad', sheet: 'ars-botanica.cc.items', position: [177, 0] },
		{ id: 'music_disc_wait', sheet: 'ars-botanica.cc.items', position: [178, 0] },
		{ id: 'music_disc_ward', sheet: 'ars-botanica.cc.items', position: [179, 0] },
		{ id: 'mutton', sheet: 'ars-botanica.cc.items', position: [180, 0] },
		{ id: 'name_tag', sheet: 'ars-botanica.cc.items', position: [181, 0] },
		{ id: 'nether_brick', sheet: 'ars-botanica.cc.items', position: [182, 0] },
		{ id: 'nether_star', sheet: 'ars-botanica.cc.items', position: [183, 0] },
		{ id: 'nether_wart', sheet: 'ars-botanica.cc.items', position: [184, 0] },
		{ id: 'oak_boat', sheet: 'ars-botanica.cc.items', position: [185, 0] },
		{ id: 'oak_door', sheet: 'ars-botanica.cc.items', position: [186, 0] },
		{ id: 'orange_dye', sheet: 'ars-botanica.cc.items', position: [187, 0] },
		{ id: 'painting', sheet: 'ars-botanica.cc.items', position: [188, 0] },
		{ id: 'paper', sheet: 'ars-botanica.cc.items', position: [189, 0] },
		{ id: 'pink_dye', sheet: 'ars-botanica.cc.items', position: [190, 0] },
		{ id: 'poisonous_potato', sheet: 'ars-botanica.cc.items', position: [191, 0] },
		{ id: 'popped_chorus_fruit', sheet: 'ars-botanica.cc.items', position: [192, 0] },
		{ id: 'porkchop', sheet: 'ars-botanica.cc.items', position: [193, 0] },
		{ id: 'potato', sheet: 'ars-botanica.cc.items', position: [194, 0] },
		{ id: 'potion_overlay', sheet: 'ars-botanica.cc.items', position: [195, 0] },
		{ id: 'potion', sheet: 'ars-botanica.cc.items', position: [196, 0] },
		{ id: 'prismarine_crystals', sheet: 'ars-botanica.cc.items', position: [197, 0] },
		{ id: 'prismarine_shard', sheet: 'ars-botanica.cc.items', position: [198, 0] },
		{ id: 'pufferfish', sheet: 'ars-botanica.cc.items', position: [199, 0] },
		{ id: 'pufferfish_bucket', sheet: 'ars-botanica.cc.items', position: [200, 0] },
		{ id: 'pumpkin_pie', sheet: 'ars-botanica.cc.items', position: [201, 0] },
		{ id: 'pumpkin_seeds', sheet: 'ars-botanica.cc.items', position: [202, 0] },
		{ id: 'purple_dye', sheet: 'ars-botanica.cc.items', position: [203, 0] },
		{ id: 'quartz', sheet: 'ars-botanica.cc.items', position: [204, 0] },
		{ id: 'quiver', sheet: 'ars-botanica.cc.items', position: [205, 0] },
		{ id: 'rabbit', sheet: 'ars-botanica.cc.items', position: [206, 0] },
		{ id: 'rabbit_foot', sheet: 'ars-botanica.cc.items', position: [207, 0] },
		{ id: 'rabbit_hide', sheet: 'ars-botanica.cc.items', position: [208, 0] },
		{ id: 'rabbit_stew', sheet: 'ars-botanica.cc.items', position: [209, 0] },
		{ id: 'redstone', sheet: 'ars-botanica.cc.items', position: [210, 0] },
		{ id: 'repeater', sheet: 'ars-botanica.cc.items', position: [211, 0] },
		{ id: 'rose_red', sheet: 'ars-botanica.cc.items', position: [212, 0] },
		{ id: 'rotten_flesh', sheet: 'ars-botanica.cc.items', position: [213, 0] },
		{ id: 'ruby', sheet: 'ars-botanica.cc.items', position: [214, 0] },
		{ id: 'saddle', sheet: 'ars-botanica.cc.items', position: [215, 0] },
		{ id: 'salmon', sheet: 'ars-botanica.cc.items', position: [216, 0] },
		{ id: 'salmon_bucket', sheet: 'ars-botanica.cc.items', position: [217, 0] },
		{ id: 'shears', sheet: 'ars-botanica.cc.items', position: [218, 0] },
		{ id: 'shulker_shell', sheet: 'ars-botanica.cc.items', position: [219, 0] },
		{ id: 'sign', sheet: 'ars-botanica.cc.items', position: [220, 0] },
		{ id: 'slime_ball', sheet: 'ars-botanica.cc.items', position: [221, 0] },
		{ id: 'snowball', sheet: 'ars-botanica.cc.items', position: [222, 0] },
		{ id: 'spawn_egg_overlay', sheet: 'ars-botanica.cc.items', position: [223, 0] },
		{ id: 'spawn_egg', sheet: 'ars-botanica.cc.items', position: [224, 0] },
		{ id: 'spectral_arrow', sheet: 'ars-botanica.cc.items', position: [225, 0] },
		{ id: 'spider_eye', sheet: 'ars-botanica.cc.items', position: [226, 0] },
		{ id: 'splash_potion', sheet: 'ars-botanica.cc.items', position: [227, 0] },
		{ id: 'spruce_boat', sheet: 'ars-botanica.cc.items', position: [228, 0] },
		{ id: 'spruce_door', sheet: 'ars-botanica.cc.items', position: [229, 0] },
		{ id: 'stick', sheet: 'ars-botanica.cc.items', position: [230, 0] },
		{ id: 'stone_axe', sheet: 'ars-botanica.cc.items', position: [231, 0] },
		{ id: 'stone_hoe', sheet: 'ars-botanica.cc.items', position: [232, 0] },
		{ id: 'stone_pickaxe', sheet: 'ars-botanica.cc.items', position: [233, 0] },
		{ id: 'stone_shovel', sheet: 'ars-botanica.cc.items', position: [234, 0] },
		{ id: 'stone_sword', sheet: 'ars-botanica.cc.items', position: [235, 0] },
		{ id: 'string', sheet: 'ars-botanica.cc.items', position: [236, 0] },
		{ id: 'structure_void', sheet: 'ars-botanica.cc.items', position: [237, 0] },
		{ id: 'sugar', sheet: 'ars-botanica.cc.items', position: [238, 0] },
		{ id: 'sugar_cane', sheet: 'ars-botanica.cc.items', position: [239, 0] },
		{ id: 'tipped_arrow_base', sheet: 'ars-botanica.cc.items', position: [240, 0] },
		{ id: 'tipped_arrow_head', sheet: 'ars-botanica.cc.items', position: [241, 0] },
		{ id: 'tnt_minecart', sheet: 'ars-botanica.cc.items', position: [242, 0] },
		{ id: 'totem_of_undying', sheet: 'ars-botanica.cc.items', position: [243, 0] },
		{ id: 'tropical_fish', sheet: 'ars-botanica.cc.items', position: [244, 0] },
		{ id: 'tropical_fish_bucket', sheet: 'ars-botanica.cc.items', position: [245, 0] },
		{ id: 'water_bucket', sheet: 'ars-botanica.cc.items', position: [246, 0] },
		{ id: 'wheat', sheet: 'ars-botanica.cc.items', position: [247, 0] },
		{ id: 'wheat_seeds', sheet: 'ars-botanica.cc.items', position: [248, 0] },
		{ id: 'wooden_axe', sheet: 'ars-botanica.cc.items', position: [249, 0] },
		{ id: 'wooden_hoe', sheet: 'ars-botanica.cc.items', position: [250, 0] },
		{ id: 'wooden_pickaxe', sheet: 'ars-botanica.cc.items', position: [251, 0] },
		{ id: 'wooden_shovel', sheet: 'ars-botanica.cc.items', position: [252, 0] },
		{ id: 'wooden_sword', sheet: 'ars-botanica.cc.items', position: [253, 0] },
		{ id: 'writable_book', sheet: 'ars-botanica.cc.items', position: [254, 0] },
		{ id: 'written_book', sheet: 'ars-botanica.cc.items', position: [255, 0] }
	];
	maps.forEach(function (m) {
		m.id = 'ars-botanica.items.cc.' + m.id;
		mappings.push(m);
	});
};
