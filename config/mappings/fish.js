module.exports = function (mappings) {
	let index = 0;
	[
		'anchovy',
		'angel',
		'angler',
		'bass',
		'blaasop',
		'bowfin',
		'butterfly',
		'carp',
		'catfish',
		'chub',
		'clown',
		'cod',
		'damsel',
		'electricray',
		'gold',
		'herring',
		'koi',
		'lamprey',
		'lungfish',
		'mantaray',
		'minnow',
		'perch',
		'pickerel',
		'pike',
		'piranha',
		'puffer',
		'pupfish',
		'salmon',
		'sardine',
		'siamese',
		'stargazer',
		'stingray',
		'tang',
		'tetra',
		'trout',
		'tuna',
		'walleye'
	].forEach(function (name) {
		mappings.push({
			id: 'ars-botanica.item.fish.' + name,
			sheet: 'ars-botanica.harvest-festival.fish',
			position: [index, 0]
		});
		index++;
	});
};
