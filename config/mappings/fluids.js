module.exports = function (mappings) {
	[
		{
			id: 'ars-botanica.items.fluids.jug.empty',
			position: [28, 1],
			sheet: 'ars-botanica.henrysoftware.items'
		},
		{
			id: 'ars-botanica.items.fluids.jug.water',
			position: [40, 1],
			sheet: 'ars-botanica.henrysoftware.items'
		},
		{
			id: 'ars-botanica.items.fluids.bottle.empty',
			position: [59, 1],
			sheet: 'ars-botanica.henrysoftware.items'
		},
		{
			id: 'ars-botanica.items.fluids.bottle.milk',
			position: [60, 1],
			sheet: 'ars-botanica.henrysoftware.items'
		}
	].forEach(function (map) {
		mappings.push(map);
	});
};
