module.exports = function (mappings) {
	[
		{ id: 'bamboo', position: [0, 0] },
		{ id: 'chili', position: [0, 4] },
		{ id: 'chinese-cabbage', position: [0, 5] },
		{ id: 'citron', position: [0, 6] },
		{ id: 'corn', position: [0, 7] },
		{ id: 'cucumber', position: [1, 1] },
		{ id: 'eggplant', position: [1, 3] },
		{ id: 'green-pepper', position: [2, 0] },
		{ id: 'leek', position: [2, 2] },
		{ id: 'lettuce', position: [2, 3] },
		{ id: 'onion', position: [2, 5] },
		{ id: 'peanut', position: [2, 6] },
		{ id: 'pickled-cabbage', position: [2, 7] },
		{ id: 'picked-cucumber', position: [3, 0] },
		{ id: 'pickled-pepper', position: [3, 1] },
		{ id: 'pickled-turnip', position: [3, 2] },
		{ id: 'pomelo', position: [3, 4] },
		{ id: 'red-pepper', position: [3, 5] },
		{ id: 'scallion', position: [4, 2] },
		{ id: 'spinach', position: [5, 1] },
		{ id: 'tangerine', position: [5, 2] },
		{ id: 'tofu', position: [5, 3] },
		{ id: 'tomato', position: [5, 4] },
		{ id: 'turnip', position: [5, 5] }

		// { id: '', position: [#, #] },
	].forEach(function (map) {
		map.id = 'ars-botanica.item.fruitveg.' + map.id;
		map.sheet = map.sheet || 'ars-botanica.cuisine.items';
		mappings.push(map);
	});
};
