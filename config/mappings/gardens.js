module.exports = function (mappings) {
	[
		{
			id: 'flower',
			position: [3, 20],
			sheet: 'isleward.tiles'
		},
		{
			id: 'vegetable',
			position: [1, 3],
			sheet: 'isleward.tiles'
		},
		{
			id: 'berry',
			cell: 224,
			sheet: 'isleward.walls'
		},
		{
			id: 'grass',
			position: [0, 7],
			sheet: 'isleward.tiles'
		},
		{
			id: 'mushroom',
			position: [4, 7],
			sheet: 'isleward.tiles'
		},
		{
			id: 'crystal',
			cell: 142,
			sheet: 'isleward.walls'
		}
	].forEach(function (map) {
		map.id = 'ars-botanica.tile.garden.' + map.id;
		mappings.push(map);
	});
};
