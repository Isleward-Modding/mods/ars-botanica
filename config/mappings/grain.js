module.exports = function (mappings) {
	[
		{ id: 'rice', position: [6, 3] },
		{ id: 'rice-powder', position: [7, 3] },
		{ id: 'white-rice', position: [0, 6] }

		// { id: '', position: [#, #] },
	].forEach(function (map) {
		map.id = 'ars-botanica.items.grains.' + map.id;
		map.sheet = map.sheet || 'ars-botanica.cuisine.items';
		mappings.push(map);
	});
};
