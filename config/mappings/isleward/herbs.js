module.exports = function (mappings) {
	[
		{ id: 'moonbell', position: [1, 1] },
		{ id: 'skyblossom', position: [1, 2] },
		{ id: 'emberleaf', position: [1, 0] }
	].forEach(function (map) {
		map.id = 'isleward.items.herbs.' + map.id;
		map.sheet = map.sheet || 'isleward.materials';
		mappings.push(map);
	});
};
