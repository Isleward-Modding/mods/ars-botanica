module.exports = function (mappings) {
	let index = 0;
	[
		'banana',
		'cabbage',
		'corn',
		'corn_alt',
		'cucumber',
		'eggplant',
		'grape',
		'grass',
		'green_pepper',
		'onion',
		'orange',
		'peach',
		'pineapple',
		'spinach',
		'strawberry',
		'sweet_potato',
		'tomato',
		'turnip',
		'tutorial_turnip'
	].forEach(function (name) {
		mappings.push({
			id: 'ars-botanica.item.harvestfestival.crop.' + name,
			sheet: 'ars-botanica.harvest-festival.crops',
			position: [0, index]
		});
		index++;
	});
};
