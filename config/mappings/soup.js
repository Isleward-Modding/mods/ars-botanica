module.exports = function (mappings) {
	[
		'empty',
		'stock',
		'soup',
		'stew'
	].forEach(function (name, index) {
		mappings.push({
			id: 'ars-botanica.items.soups.' + name,
			sheet: 'ars-botanica.soup',
			position: [index, 0]
		});
	});
};
