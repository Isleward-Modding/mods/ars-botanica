module.exports = function (mappings) {
	[
		{ id: 'chili-powder', position: [4, 0] },
		{ id: 'salt-crystals', position: [0, 1] },
		{ id: 'garlic', position: [6, 1] },
		{ id: 'ginger', position: [7, 1] },
		{ id: 'onion', position: [5, 2] },
		{ id: 'salt', position: [0, 4] },
		{ id: 'sesame', position: [3, 4] },
		{ id: 'sichuan-pepper', position: [4, 4] },
		{ id: 'sichuan-pepper-powder', position: [5, 4] },
		{ id: 'spice-bottle', position: [7, 4] },
		{ id: 'brown-sugar', position: [6, 5] }

		// { id: '', position: [#, #] },
	].forEach(function (map) {
		map.id = 'ars-botanica.items.spices.' + map.id;
		map.sheet = map.sheet || 'ars-botanica.cuisine.items';
		mappings.push(map);
	});
};
