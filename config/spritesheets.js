module.exports = [
	// base game sheets that we... repurpose
	{
		id: 'isleward.tiles',
		path: 'images/tiles.png',
		width: 8
	},
	{
		id: 'isleward.walls',
		path: 'images/walls.png',
		width: 8
	},
	{
		id: 'isleward.materials',
		path: 'images/materials.png',
		width: 12
	},

	// custom spritesheets
	{
		id: 'ars-botanica.bf',
		path: 'server/mods/ars-botanica/assets/birds-foods/sheet.png',
		width: 8
	},

	{
		id: 'ars-botanica.henrysoftware.items',
		path: 'server/mods/ars-botanica/assets/henrysoftware/items.png',
		width: 64
	},

	{
		id: 'ars-botanica.cuisine.items',
		path: 'server/mods/ars-botanica/assets/cuisine/items.png',
		width: 8
	},

	{
		id: 'ars-botanica.harvest-festival.fish',
		path: 'server/mods/ars-botanica/assets/harvest-festival/fish.png',
		width: 37
	},
	{
		id: 'ars-botanica.harvest-festival.crops',
		path: 'server/mods/ars-botanica/assets/harvest-festival/crops.png',
		width: 19
	},
	{
		id: 'ars-botanica.harvest-festival.items',
		path: 'server/mods/ars-botanica/assets/harvest-festival/items.png',
		width: 10
	},

	{
		id: 'ars-botanica.cc.items',
		path: 'server/mods/ars-botanica/assets/coterie-craft/items.png',
		width: 256
	},

	{
		id: 'ars-botanica.soup',
		path: 'server/mods/ars-botanica/assets/custom/soup.png',
		width: 4
	}
];
