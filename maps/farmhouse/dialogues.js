module.exports = {
	farmhand: {
		1: {
			msg: [{
				msg: 'Hmm? Oh, hello. What\'d you need?',
				options: [1.1, 1.2, 1.3, 1.4]
			}],
			options: {
				1.1: {
					msg: 'Why do you live out here in the wilderness?',
					goto: 2
				},
				1.2: {
					msg: 'Got anything for sale?',
					goto: 'tradeBuy'
				}
			}
		},
		2: {
			msg: 'I moved out here from the city to help out my old friend, the hermit next door, with the farm.',
			options: {
				2.1: {
					msg: 'How do you get all your supplies?',
					goto: '2-1'
				},
				2.2: {
					msg: 'Where is the city?',
					goto: '2-2'
				},
				2.3: {
					msg: 'I\'d like to ask something else.',
					goto: 1
				}
			}
		},
		'2-1': {
			msg: 'We grow, harvest, and scavenge what we can, and sometimes we secretly buy supplies from citizens who happen to wander out this far.',
			options: {
				'2-1.1': {
					msg: 'What can you produce for yourself out here?',
					goto: '2-1-1'
				},
				'2-1.2': {
					msg: 'I\'d like to ask something else.',
					goto: 1
				}
			}
		},
		'2-1-1': {
			msg: 'There\'s a few farm animals out back, and I tend to some crops and gather from the plants around the island as well. If you\'ve got some gold, I can sell you some for cooking, since you\'re shipwrecked here and all.',
			options: {
				'2-1-1.1': {
					msg: 'What do you have for sale?',
					goto: 'tradeBuy'
				},
				'2-1-1.2': {
					msg: 'I\'d like to ask something else.',
					goto: 1
				}
			}
		},
		'2-2': {
			msg: 'It\'s north of here. The hermit can\'t move back because of some... conflicts, so we\'re both living out here now.',
			options: {
				'2-2.1': {
					msg: 'I\'d like to ask something else.',
					goto: 2
				}
			}
		},
		tradeBuy: {
			cpn: 'trade',
			method: 'startBuy',
			args: [{
				targetName: 'farmhand'
			}]
		}
	}
};
