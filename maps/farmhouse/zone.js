module.exports = {
	mobs: {
		farmhand: {
			level: 10,
			walkDistance: 0,
			attackable: false,
			regular: {
				drops: {
					chance: 75,
					rolls: 1
				}
			},
			rare: {
				count: 0
			},

			properties: {
				cpnTrade: {
					items: {
						min: 0,
						max: 0
					},
					forceItems: [
						// populated in code to be able to use Asset Atlas
					],
					level: {
						min: 1,
						max: 5
					},
					markup: {
						buy: 0.25,
						sell: 2.5
					}
				}
			}
		}
	},
	objects: {
		bucket: {
			components: {
				cpnWorkbench: {
					type: 'well'
				}
			}
		},
		'cooking pot': {
			components: {
				cpnWorkbench: {
					type: 'pot'
				},
				cpnParticles: {
					simplify: function () {
						return {
							type: 'particles',
							blueprint: {
								color: {
									start: ['808080', '808080'],
									end: ['d3d3d3', 'd3d3d3']
								},
								scale: {
									start: {
										min: 2,
										max: 10
									},
									end: {
										min: 0,
										max: 2
									}
								},
								speed: {
									start: {
										min: 4,
										max: 16
									},
									end: {
										min: 2,
										max: 8
									}
								},
								lifetime: {
									min: 1,
									max: 4
								},
								randomScale: true,
								randomSpeed: true,
								chance: 0.1,
								randomColor: true,
								spawnType: 'rect',
								spawnRect: {
									x: -15,
									y: -20,
									w: 30,
									h: 8
								}
							}
						};
					}
				}
			}
		},
		'food workstation': {
			components: {
				cpnWorkbench: {
					type: 'foodprep'
				}
			}
		},
		'saltwater station': {
			components: {
				cpnWorkbench: {
					type: 'saltwater'
				}
			}
		},
		shopfarmhand: {
			properties: {
				cpnNotice: {
					actions: {
						enter: {
							cpn: 'dialogue',
							method: 'talk',
							args: [{
								targetName: 'farmhand'
							}]
						},
						exit: {
							cpn: 'dialogue',
							method: 'stopTalk'
						}
					}
				}
			}
		}
	}
};
