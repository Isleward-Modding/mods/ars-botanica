module.exports = function (AA) {
	return [{
		id: 'baking.fries',
		type: ['cooking'],
		inherit: ['base'],
		name: 'Cook Fries',
		description: 'Cut some potatoes, add salt, and make fries.',
		inputs: [{
			name: 'Potato',
			quantity: 3
		}, {
			name: 'Salt',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Fries',
			sprite: AA.getAsset('ars-botanica.items.bf.hot_chips').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.bf.hot_chips').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 15
				}
			}]
		}]
	}, {
		id: 'poutine',
		type: ['foodprep'],
		inherit: ['base'],
		name: 'Make Poutine',
		description: 'Add cheese and gravy to fries.',
		inputs: [{
			name: 'Fries',
			quantity: 1
		}, {
			name: 'Cheese',
			quantity: 1
		}, {
			name: 'Gravy',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Poutine',
			sprite: AA.getAsset('ars-botanica.items.bf.poutine').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.bf.poutine').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 40
				}
			}]
		}]
	},

	{
		id: 'baking.dough',
		type: ['foodprep', '*'],
		inherit: ['base'],
		name: 'Make Dough',
		description: 'Use some flour, salt, and water to make some general-purpose dough.',
		inputs: [{
			name: 'Flour',
			quantity: 1
		}, {
			name: 'Salt',
			quantity: 1
		}, {
			name: 'Water Jug',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Dough',
			sprite: AA.getAsset('ars-botanica.items.baking.dough').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.baking.dough').path
		}, {
			inherit: ['attribute.material'],
			name: 'Empty Jug',
			sprite: AA.getAsset('ars-botanica.items.fluids.jug.empty').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.jug.empty').path
		}]
	}, {
		id: 'baking.bread',
		name: 'Bake Bread',
		description: 'Heat some dough into bread.',
		type: ['cooking'],
		inherit: ['attribute.consumable'],
		inputs: [{
			name: 'Dough',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.consumable'],
			name: 'Bread',
			// description: 'A freshly baked loaf of bread.',
			sprite: AA.getAsset('ars-botanica.items.baking.bread').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.baking.bread').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 25
				}
			}]
		}]
	}, {
		id: 'baking.biscuit',
		name: 'Bake Biscuits',
		description: 'Bake some dough and butter into 3 biscuits.',
		type: ['cooking'],
		inherit: ['attribute.consumable'],
		inputs: [{
			name: 'Dough',
			quantity: 1
		}, {
			name: 'Butter',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.consumable'],
			name: 'Biscuit',
			quantity: 3,
			sprite: AA.getAsset('ars-botanica.items.cc.bread').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.cc.bread').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 10
				}
			}]
		}]
	}, {
		id: 'baking.dough.cookie',
		type: ['foodprep', '*'],
		inherit: ['base'],
		name: 'Make Cookie Dough',
		description: 'Mix some flour, salt, chocolate, sugar, egg, and butter to make cookie dough. Makes enough dough for 10 cookies.',
		inputs: [{
			name: 'Flour',
			quantity: 2
		}, {
			name: 'Salt',
			quantity: 1
		}, {
			name: 'Chocolate',
			quantity: 1
		}, {
			name: 'Sugar',
			quantity: 1
		}, {
			name: 'Egg',
			quantity: 1
		}, {
			name: 'Butter',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Cookie Dough',
			// quantity: 1,
			// description: 'A simple dough for baking',
			sprite: AA.getAsset('ars-botanica.items.baking.dough').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.baking.dough').path
		}]
	}, {
		id: 'baking.cookie',
		name: 'Bake Cookies',
		description: 'Bake some cookie dough into cookies.',
		type: ['cooking'],
		inherit: ['attribute.consumable'],
		inputs: [{
			name: 'Cookie Dough',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.consumable'],
			name: 'Cookie',
			quantity: 10,
			// description: 'A freshly baked loaf of bread.',
			sprite: AA.getAsset('ars-botanica.items.cookie').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.cookie').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 10
				}
			}]
		}]
	}];
};
