module.exports = function (AA) {
	return [{
		id: 'dairy.butter',
		type: ['foodprep'],
		inherit: ['base'],
		name: 'Churn Butter',
		description: 'Make butter.',
		inputs: [{
			name: 'Bottle of Milk',
			quantity: 1
		}, {
			name: 'Salt',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Butter',
			sprite: AA.getAsset('ars-botanica.items.butter').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.butter').path
		}, {
			inherit: ['attribute.material'],
			name: 'Empty Bottle',
			sprite: AA.getAsset('ars-botanica.items.fluids.bottle.empty').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.bottle.empty').path,
			type: 'container',
			harvestType: 'fluid',
			useText: 'fill'
		}]
	}, {
		id: 'dairy.cheese',
		type: ['foodprep'],
		inherit: ['base'],
		name: 'Make Cheese',
		description: 'Make cheese.',
		inputs: [{
			name: 'Bottle of Milk',
			quantity: 1
		}, {
			name: 'Salt',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Cheese',
			sprite: AA.getAsset('ars-botanica.items.cheese').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.cheese').path
		}, {
			inherit: ['attribute.material'],
			name: 'Empty Bottle',
			sprite: AA.getAsset('ars-botanica.items.fluids.bottle.empty').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.bottle.empty').path,
			type: 'container',
			harvestType: 'fluid',
			useText: 'fill'
		}]
	}];
};
