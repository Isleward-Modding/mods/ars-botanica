module.exports = function (AA) {
	return [{
		id: 'egg.fry',
		type: ['cooking'],
		inherit: ['base'],
		name: 'Fry Eggs',
		description: 'Fry two eggs.',
		inputs: [{
			name: 'Egg',
			quantity: 2
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Fried Eggs',
			sprite: AA.getAsset('ars-botanica.items.egg-cooked').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.egg-cooked').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 20
				}
			}]
		}]
	}];
};
