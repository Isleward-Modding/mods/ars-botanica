module.exports = function (AA) {
	return [
		// water bucketing
		{
			id: 'fluids.water.drain',
			name: 'Empty Water Jug',
			description: 'Empty a water jug.',
			type: ['well', 'saltwater'],
			inherit: ['base'],
			inputs: [{
				name: 'Water Jug',
				quantity: 1
			}],
			outputs: [{
				inherit: ['attribute.material'],
				name: 'Empty Jug',
				// description: 'Ready to be filled.',
				sprite: AA.getAsset('ars-botanica.items.fluids.jug.empty').position,
				spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.jug.empty').path
			}]
		},
		{
			id: 'fluids.water.fill',
			name: 'Fill Water Jug',
			description: 'Fill a water jug.',
			type: ['well', 'saltwater'],
			inherit: ['base'],
			inputs: [{
				name: 'Empty Jug',
				quantity: 1
			}],
			outputs: [{
				inherit: ['attribute.material'],
				name: 'Water Jug',
				// description: 'Filled with fresh well water.',
				sprite: AA.getAsset('ars-botanica.items.fluids.jug.water').position,
				spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.jug.water').path
			}]
		},

		// saltwater
		{
			id: 'fluids.water.evaporate',
			name: 'Evaporate Saltwater',
			description: 'Pour out some water (mixed in with seawater) on the processing table. As it evaporates, salt will be left behind.',
			type: ['saltwater'],
			inherit: ['base'],
			inputs: [{
				name: 'Water Jug',
				quantity: 1
			}],
			outputs: [{
				inherit: ['attribute.material'],
				name: 'Salt',
				// description: '',
				sprite: AA.getAsset('ars-botanica.items.spices.salt').position,
				spritesheet: AA.getAssetSpritesheet('ars-botanica.items.spices.salt').path
			}, {
				// Copy empty jug item
				inherit: ['fluids.water.drain:output:0']
			}]
		},

		// milk dump in well?!?!?
		{
			id: 'fluids.milk.drain',
			name: 'Empty Bottle of Milk',
			description: 'Empty a bottle of milk into the well (why?).',
			type: ['well'],
			inherit: ['base'],
			inputs: [{
				name: 'Bottle of Milk',
				quantity: 1
			}],
			outputs: [{
				inherit: ['attribute.material'],
				name: 'Empty Bottle',
				// description: 'Ready to be filled.',
				sprite: AA.getAsset('ars-botanica.items.fluids.bottle.empty').position,
				spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.bottle.empty').path,

				// Milk fillable stuff
				type: 'container',
				harvestType: 'fluid',
				useText: 'fill'
			}]
		}
	];
};
