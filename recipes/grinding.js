module.exports = function (AA) {
	return [{
		id: 'grinding.base',
		// allowed at foodprep station because grinding station isn't in game
		type: ['grinding', 'foodprep'],
		inherit: ['base'],
		inputs: [],
		outputs: []
	}, {
		id: 'grinding.wheat.flour',
		name: 'Grind Wheat',
		description: 'Grind wheat into flour for baking.',
		type: [],
		inherit: ['grinding.base'],
		inputs: [{
			name: 'Wheat',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Flour',
			// description: 'Finely ground wheat flour',
			sprite: AA.getAsset('ars-botanica.items.baking.flour').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.baking.flour').path
		}]
	}, {
		id: 'grinding.sugarcane.sugar',
		name: 'Grind Sugarcane',
		description: 'Grind sugarcane into sugar.',
		type: [],
		inherit: ['grinding.base'],
		inputs: [{
			name: 'Sugarcane',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Sugar',
			sprite: AA.getAsset('ars-botanica.items.cc.sugar').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.cc.sugar').path
		}]
	}];
};
