module.exports = function (AA) {
	return [{
		id: 'icecream',
		type: ['foodprep'],
		inherit: ['base'],
		name: 'Make Ice Cream',
		description: 'Make some unflavored ice cream base with ice, salt, and milk.',
		inputs: [{
			name: 'Ice',
			quantity: 3
		}, {
			name: 'Bottle of Milk',
			quantity: 1
		}, {
			name: 'Salt',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Ice Cream',
			sprite: AA.getAsset('ars-botanica.items.icecream').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.icecream').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 10
				}
			}]
		}, {
			inherit: ['attribute.material'],
			name: 'Empty Bottle',
			sprite: AA.getAsset('ars-botanica.items.fluids.bottle.empty').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.bottle.empty').path,
			type: 'container',
			harvestType: 'fluid',
			useText: 'fill'
		}]
	}];
};
