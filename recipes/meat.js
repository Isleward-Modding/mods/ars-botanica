module.exports = function (AA) {
	return [{
		id: 'meat',
		type: ['cooking'],
		inherit: ['base'],
		name: 'Cook Meat',
		description: 'Cook raw meat into cooked meat. This will make the meat unusable for most other recipes, but consumable on its own.',
		inputs: [{
			name: 'Meat',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Cooked Meat',
			sprite: AA.getAsset('ars-botanica.items.cc.cooked_porkchop').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.cc.cooked_porkchop').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 15
				}
			}]
		}]
	}];
};
