module.exports = function (AA) {
	return [{
		id: 'pot.saltwater',
		type: ['pot'],
		inherit: ['base'],
		name: 'Boil Saltwater',
		description: 'Boil saltwater to evaporate the water, leaving salt behind.',
		inputs: [{
			name: 'Water Jug',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Salt',
			sprite: AA.getAsset('ars-botanica.items.spices.salt').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.spices.salt').path
		}, {
			inherit: ['attribute.material'],
			name: 'Empty Jug',
			sprite: AA.getAsset('ars-botanica.items.fluids.jug.empty').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.jug.empty').path
		}]
	}, {
		id: 'pot.stock',
		type: ['pot'],
		inherit: ['base'],
		name: 'Cook Stock',
		description: 'Cook some stock to be used later in other soups.',
		inputs: [{
			name: 'Water Jug',
			quantity: 3
		}, {
			name: 'Bone',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Stock',
			quantity: 3,
			sprite: AA.getAsset('ars-botanica.items.soups.stock').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.soups.stock').path
		}, {
			inherit: ['attribute.material'],
			name: 'Empty Jug',
			quantity: 3,
			sprite: AA.getAsset('ars-botanica.items.fluids.jug.empty').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.fluids.jug.empty').path
		}]
	}, {
		id: 'pot.gravy',
		type: ['pot'],
		inherit: ['base'],
		name: 'Make Gravy',
		description: 'Thicken some stock to make gravy.',
		inputs: [{
			name: 'Stock',
			quantity: 1
		}, {
			name: 'Flour',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material'],
			name: 'Gravy',
			sprite: AA.getAsset('ars-botanica.items.bf.gravy').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.bf.gravy').path
		}]
	}, {
		id: 'mashedpotato',
		type: ['foodprep'],
		inherit: ['base'],
		name: 'Make Mashed Potatoes',
		description: 'Mash some potatoes.',
		inputs: [{
			name: 'Potato',
			quantity: 3
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Mashed Potatoes',
			sprite: AA.getAsset('ars-botanica.items.bf.mashed_potato').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.bf.mashed_potato').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 20
				}
			}]
		}]
	}, {
		id: 'mashedpotato.gravy',
		type: ['foodprep'],
		inherit: ['base'],
		name: 'Make Mashed Potatoes and Gravy',
		description: 'Add some gravy to mashed potatoes.',
		inputs: [{
			name: 'Mashed Potatoes',
			quantity: 1
		}, {
			name: 'Gravy',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Mashed Potatoes and Gravy',
			sprite: AA.getAsset('ars-botanica.items.bf.mashed_potato_gravy').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.bf.mashed_potato_gravy').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 40
				}
			}]
		}]
	}, {
		id: 'pot.stew',
		type: ['pot'],
		inherit: ['base'],
		name: 'Cook Meat Stew',
		description: 'Make a stew with meat, flour, and stock.',
		inputs: [{
			name: 'Stock',
			quantity: 1
		}, {
			name: 'Meat',
			quantity: 1
		}, {
			name: 'Flour',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Meat Stew',
			quantity: 1,
			sprite: AA.getAsset('ars-botanica.items.soups.stew').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.soups.stew').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 35
				}
			}]
		}]
	}, {
		id: 'pot.potroast',
		type: ['pot'],
		inherit: ['base'],
		name: 'Cook Pot Roast',
		description: 'Make a pot roast with meat, stock, and vegetables.',
		inputs: [{
			name: 'Stock',
			quantity: 1
		}, {
			name: 'Meat',
			quantity: 2
		}, {
			name: 'Carrot',
			quantity: 1
		}, {
			name: 'Potato',
			quantity: 1
		}],
		outputs: [{
			inherit: ['attribute.material', 'attribute.consumable'],
			name: 'Pot Roast',
			quantity: 1,
			sprite: AA.getAsset('ars-botanica.items.soups.stew').position,
			spritesheet: AA.getAssetSpritesheet('ars-botanica.items.soups.stew').path,
			effects: [{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: 60
				}
			}]
		}]
	}];
};
