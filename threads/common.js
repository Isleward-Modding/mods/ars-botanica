// Make sure the atlas is loaded
const deferToAtlas = function (func, obj) {
	if (!obj.assetAtlas) {
		setTimeout(function () {
			deferToAtlas(func, obj);
		}, 50);
	} else
		func();
};

module.exports = {
	initCommon: function () {
		this.events.on('better-recipes:onRecipesReady', this.onRecipesReady.bind(this));

		this.events.on('asset-atlas:onGetSpritesheets', this.onGetSpritesheets.bind(this));
		this.events.on('asset-atlas:onGetMappings', this.onGetMappings.bind(this));
		this.events.on('asset-atlas:onAtlasReady', this.onAtlasReady.bind(this));
	},

	onGetSpritesheets: function (spritesheets) {
		this.require('config/spritesheets', true).forEach(function (sheet) {
			spritesheets.push(sheet);
		});
	},
	onGetMappings: function (mappings) {
		this.require('config/mappings', true).bind(this)(mappings);
	},
	onAtlasReady: function (assetAtlas) {
		this.assetAtlas = assetAtlas;
	},

	onRecipesReady: function (recipes) {
		// DISABLE: use foodprep station for grinding
		// recipes.buildRecipe({
		// 	id: 'tool.hand-grindstone',
		// 	name: 'Spawn: Hand Grindstone',
		// 	description: 'CHEAT IN a hand grindstone for free. THIS SHOULD ONLY BE VISIBLE IN TESTING',
		// 	// HACK crafted at a cooking station
		// 	type: ['cooking', '*'],
		// 	inherit: [],
		// 	inputs: [],
		// 	outputs: [{
		// 		name: 'Hand Grindstone',
		// 		description: 'Grind stuff?',
		// 		type: 'workbench',
		// 		sprite: [0, 0],
		// 		spritesheet: 'images/materials.png',
		// 		worth: 0,
		// 		noSalvage: true,
		// 		noAugment: true,
		// 		useText: 'activate',
		// 		effects: [{
		// 			type: 'crafting',
		// 			rolls: {
		// 				workbench: 'grinding'
		// 			}
		// 		}]
		// 	}]
		// });
		recipes.buildRecipe({
			id: 'tool.all-crafter',
			name: 'Spawn: All Crafter',
			description: 'CHEAT IN a debug item that can craft any Ars Botanica item. TESTING ONLY',
			// HACK can be made at cooking station (hermit fire)
			type: ['cooking', '*'],
			inherit: [],
			inputs: [],
			outputs: [{
				name: 'All Crafter',
				description: 'Craft ANYTHING',
				type: 'workbench',
				sprite: [0, 6],
				spritesheet: 'images/materials.png',
				worth: 0,
				noSalvage: true,
				noAugment: true,
				useText: 'activate',
				effects: [{
					type: 'crafting',
					rolls: {
						workbench: '*'
					}
				}]
			}]
		});

		deferToAtlas(function () {
			[
				'baking',
				'dairy',
				'egg',
				'fluids',
				'grinding',
				'icecream',
				'meat',
				'pot'
			].forEach(function (file) {
				let f = this.require('recipes/' + file, true)(this.assetAtlas);
				f.forEach(function (recipe) {
					recipes.buildRecipe(recipe);
				}, this);
			}, this);
		}.bind(this), this);
	}
};
