module.exports = {
	playerWorkbenches: {},

	mapOffset: {
		x: 125,
		y: 118
	},
	mapFile: null,
	mapW: null,
	mapH: null,

	init: function () {
		this.mapFile = this.require('maps/farmhouse/map', true);
		this.mapW = this.mapFile.width;
		this.mapH = this.mapFile.height;

		this.events.on('onBeforeGetHerbConfig', this.onBeforeGetHerbConfig.bind(this));
		this.events.on('onBeforeUseItem', this.onBeforeUseItem.bind(this));
		this.events.on('beforeGatherResource', this.beforeGatherResource.bind(this));

		this.events.on('onBeforeGetFactions', this.onBeforeGetFactions.bind(this));
		//this.events.on('onBeforeGetEventList', this.onBeforeGetEventList.bind(this));

		//Disable when not active (not loading map)
		this.events.on('onBeforeGetDialogue', this.onBeforeGetDialogue.bind(this));
		this.events.on('onAfterGetZone', this.onAfterGetZone.bind(this));
		this.events.on('onBeforeBuildLayerTile', this.onBeforeBuildLayerTile.bind(this));
		this.events.on('onAfterGetLayerObjects', this.onAfterGetLayerObjects.bind(this));
	},

	gardenConfig: {
		'Flower Garden': {
			drops: [
				{ name: 'Moonbell', asset: 'isleward.items.herbs.moonbell' },
				{ name: 'Skyblossom', asset: 'isleward.items.herbs.skyblossom' },
				{ name: 'Emberleaf', asset: 'isleward.items.herbs.emberleaf' }
			],
			xp: 25
		},
		'Grass Garden': {
			drops: [
				{ name: 'Wheat', asset: 'ars-botanica.items.cc.wheat' },
				{ name: 'Sugarcane', asset: 'ars-botanica.items.cc.sugar_cane' }
			],
			xp: 25
		},
		'Vegetable Garden': {
			drops: [
				{ name: 'Carrot', asset: 'ars-botanica.items.cc.carrot' },
				{ name: 'Potato', asset: 'ars-botanica.items.cc.potato' }
			],
			xp: 25
		}
	},
	beforeGatherResource: function (result, obj) {
		let config = this.gardenConfig[result.obj.name];
		if (!config)
			return;

		result.xp = config.xp || result.xp;
		let rolled = config.drops[Math.floor(Math.random() * config.drops.length)];

		// Turn into an array... I think this is because drops were originally going to be arrays of items.
		rolled = [rolled];

		rolled = rolled.map(function (drop) {
			let out = extend({
				material: true,
				type: null,
				quantity: 1,
				quality: 0
			}, {
				name: drop.name,
				quantity: drop.quantity,
				quality: drop.quality
			});
			out.sprite = this.assetAtlas.getAsset(drop.asset).position;
			out.spritesheet = this.assetAtlas.getAssetSpritesheet(drop.asset).path;

			return out;
		}, this);
		result.items = rolled;
	},
	onBeforeGetFactions: function (mappings) {
		extend(mappings, {
			guildFarmers: '../mods/ars-botanica/factions/farmers',
			guildCooks: '../mods/ars-botanica/factions/cooks'
		});
	},
	onAfterGetLayerObjects: function (info) {
		if (info.map === 'fjolarok') {
			// edit objects
			info.objects.forEach(function (layerObj) {
				let handler = {
					782: function (obj) {
						obj.name = 'fountain|fountain';
					}
				}[layerObj.id] || function () {};
				handler.bind(this)(layerObj);
			}, this);

			// load map
			let layer = this.mapFile.layers.find(l => (l.name === info.layer));
			if (layer) {
				let offset = this.mapOffset;
				let mapScale = this.mapFile.tilesets[0].tileheight;

				layer.objects.forEach(function (l) {
					let newO = extend({}, l);
					newO.x += (offset.x * mapScale);
					newO.y += (offset.y * mapScale);

					info.objects.push(newO);
				}, this);
			}
		}
	},
	// juxtapose map
	onBeforeBuildLayerTile: function (info) {
		if (info.map !== 'fjolarok')
			return;

		let offset = this.mapOffset;

		let x = info.x;
		let y = info.y;

		if ((x - offset.x < 0) || (y - offset.y < 0) || (x - offset.x >= this.mapW) || (y - offset.y >= this.mapH))
			return;

		let i = ((y - offset.y) * this.mapW) + (x - offset.x);
		let layer = this.mapFile.layers.find(l => (l.name === info.layer));
		if (layer)
			info.cell = layer.data[i];
	},

	onAfterGetZone: function (zoneName, zone) {
		if (zoneName === 'fjolarok') {
			extend(zone, this.require('maps/farmhouse/zone', true));

			// Custom resources spawners
			zone.resources['Flower Garden'] = {
				type: 'herb',
				max: 10
			};
			zone.resources['Grass Garden'] = {
				type: 'herb',
				max: 15
			};
			zone.resources['Vegetable Garden'] = {
				type: 'herb',
				max: 20
			};

			// Add well to fountain in town
			zone.objects.fountain.components.cpnWorkbench = {
				type: 'well'
			};

			// Custom drops
			zone.mobs.seagull.regular.drops = extend({}, zone.mobs.seagull.regular.drops, {
				noRandom: true,
				alsoRandom: true,
				blueprints: [{
					chance: 100,
					name: 'Egg',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.egg').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.egg').path,
					material: true
				}, {
					chance: 100,
					name: 'Bone',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.bone').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.bone').path,
					material: true
				}]
			});
			zone.mobs.bunny.regular.drops = extend({}, zone.mobs.bunny.regular.drops, {
				noRandom: true,
				alsoRandom: true,
				blueprints: [{
					chance: 100,
					name: 'Meat',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.porkchop').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.porkchop').path,
					material: true
				}, {
					chance: 100,
					name: 'Bone',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.bone').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.bone').path,
					material: true
				}]
			});
			zone.mobs.elk.regular.drops = extend({}, zone.mobs.elk.regular.drops, {
				noRandom: true,
				alsoRandom: true,
				blueprints: [{
					chance: 100,
					name: 'Meat',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.porkchop').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.porkchop').path,
					material: true
				}, {
					chance: 100,
					name: 'Bone',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.bone').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.bone').path,
					material: true
				}]
			});
			zone.mobs.flamingo.regular.drops = extend({}, zone.mobs.flamingo.regular.drops, {
				noRandom: true,
				alsoRandom: true,
				blueprints: [{
					chance: 100,
					name: 'Bone',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.bone').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.bone').path,
					material: true
				}]
			});
			zone.mobs.eagle.regular = zone.mobs.eagle.regular || {};
			zone.mobs.eagle.regular.drops = extend({}, zone.mobs.eagle.regular.drops, {
				noRandom: true,
				alsoRandom: true,
				blueprints: [{
					chance: 100,
					name: 'Bone',
					sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.bone').position,
					spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.bone').path,
					material: true
				}]
			});

			// Add fill-able component to cows with milk.
			zone.mobs.cow.properties = zone.mobs.cow.properties || {};
			zone.mobs.cow.properties.cpnHarvestable = {
				harvestType: 'fluid',
				to: 'Bottle of Milk',
				toSprite: 'ars-botanica.items.fluids.bottle.milk'
			};

			// Add farmhand trades
			zone.mobs.farmhand.properties.cpnTrade.forceItems.push({
				name: 'Empty Jug',
				sprite: this.assetAtlas.getAsset('ars-botanica.items.fluids.jug.empty').position,
				spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.fluids.jug.empty').path,
				material: true,

				// TODO change this to something real
				worth: 0,
				infinite: true
			});
			zone.mobs.farmhand.properties.cpnTrade.forceItems.push({
				name: 'Empty Bottle',
				sprite: this.assetAtlas.getAsset('ars-botanica.items.fluids.bottle.empty').position,
				spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.fluids.bottle.empty').path,
				type: 'container',
				harvestType: 'fluid',
				useText: 'fill',
				material: true,

				// TODO change this to something real
				worth: 0,
				infinite: true
			});
			zone.mobs.farmhand.properties.cpnTrade.forceItems.push({
				name: 'Ice',
				sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.snowball').position,
				spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.snowball').path,
				material: true,

				// TODO change this to something real
				worth: 0,
				infinite: true
			});
			// zone.mobs.farmhand.properties.cpnTrade.forceItems.push({
			// 	name: 'Sugar',
			// 	sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.sugar').position,
			// 	spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.sugar').path,
			// 	material: true,
			//
			// 	// TODO change this to something real
			// 	worth: 0,
			// 	infinite: true
			// });
			zone.mobs.farmhand.properties.cpnTrade.forceItems.push({
				name: 'Egg',
				sprite: this.assetAtlas.getAsset('ars-botanica.items.cc.egg').position,
				spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.cc.egg').path,
				material: true,

				// TODO change this to something real
				worth: 0,
				infinite: true
			});
			zone.mobs.farmhand.properties.cpnTrade.forceItems.push({
				name: 'Chocolate',
				sprite: this.assetAtlas.getAsset('ars-botanica.items.chocolate').position,
				spritesheet: this.assetAtlas.getAssetSpritesheet('ars-botanica.items.chocolate').path,
				material: true,

				type: 'consumable',
				uses: 1,
				cdMax: 14,
				effects: [{
					type: 'gainStat',
					rolls: {
						stat: 'hp',
						amount: 5
					}
				}],

				// TODO change this to something real
				worth: 0,
				infinite: true
			});
		}
	},
	onBeforeGetDialogue: function (zone, config) {
		if (zone === 'fjolarok') {
			// load our new dialogues
			extend(config, this.require('maps/farmhouse/dialogues', true));
		}
	},
	onBeforeGetHerbConfig: function (herbs) {
		herbs['Flower Garden'] = {
			sheetName: 'tiles',
			cell: 163,
			// doesn't matter
			itemSprite: [0, 0]
		};

		herbs['Grass Garden'] = {
			sheetName: 'tiles',
			cell: 56,
			// doesn't matter
			itemSprite: [0, 0]
		};

		herbs['Vegetable Garden'] = {
			sheetName: 'tiles',
			cell: 25,
			// doesn't matter
			itemSprite: [0, 0]
		};

		/*
		{ init: [Function: init],
		  Moonbell: { sheetName: 'tiles', cell: 50, itemSprite: [ 1, 1 ] },
		  Skyblossom: { sheetName: 'tiles', cell: 52, itemSprite: [ 1, 2 ] },
		  Emberleaf: { sheetName: 'tiles', cell: 51, itemSprite: [ 1, 0 ] },
		  'Sun Carp':
		   { sheetName: 'objects',
		     itemSprite: [ 11, 2 ],
		     baseWeight: 3,
		     ttl: 30 } }
		*/
	},
	onBeforeUseItem: function (obj, item, result) {
		let physics = obj.instance.physics;

		if (item.type === 'workbench') {
			let workbench = this.playerWorkbenches[obj.serverId];
			if (!workbench) {
				workbench = this.require('objects/objects').build();

				let template = extend({}, this.require('mock/workbench', true));
				template.obj = workbench;

				workbench.components.push(template);
				workbench.workbench = template;

				//template.init({
				//	type: item.effects[0].rolls.workbench
				//});

				this.playerWorkbenches[obj.serverId] = workbench;
			}

			workbench.workbench.craftType = item.effects[0].rolls.workbench;
			workbench.workbench.open(obj);
		} else if (item.harvestType) {
			// Variables
			let x = obj.x;
			let y = obj.y;
			let radius = 1;

			// Iterate in a square around player
			for (let xx = x - radius; xx <= x + radius; xx++) {
				for (let yy = y - radius; yy <= y + radius; yy++) {
					// Ensure they have line of sight (for larger radius)
					if (!physics.hasLos(~~x, ~~y, ~~xx, ~~yy))
						continue;

					let mobs = physics.getCell(xx, yy);
					let mLen = mobs.length;

					for (let i = 0; i < mLen; i++) {
						let m = mobs[i];

						// Maybe something was killed/undefined for some reason?
						if (!m) {
							mLen--;
							continue;
						}

						// Only process if they have the cpnFiller component
						if (!m.has('harvestable'))
							continue;

						let harvest = m.harvestable;

						// Make sure harvestType matches (types will be like tapping trees, shearing sheep, collecting milk, etc)
						if (harvest.harvestType !== item.harvestType)
							continue;

						// Remove the original used item
						obj.inventory.destroyItem(item.id, 1);

						// Give the new item
						obj.inventory.getItem({
							name: harvest.to,
							// description: ''
							material: true,
							sprite: this.assetAtlas.getAsset(harvest.toSprite).position,
							spritesheet: this.assetAtlas.getAssetSpritesheet(harvest.toSprite).path
						});
					}
				}
			}
		}
	}
};
