const WIDTH = 8;
console.log('CURRENT CONFIG: Width: ' + WIDTH);

let readline = require('readline');
let rl = readline.createInterface(process.stdin, process.stdout);
rl.setPrompt('\n\ninput> ');
rl.prompt();
rl.on('line', function (line) {
	let v;
	try {
		v = eval(line);
	} catch (e) {
		console.log('Failed to parse input');
		rl.prompt();
		return;
	}

	if (typeof v === 'undefined') {
		console.log('Failed to parse input');
		rl.prompt();
		return;
	}

	if (typeof v === 'number') {
		let x = v % WIDTH;
		let y = Math.floor(v / WIDTH);
		console.log('Position | x: ' + x + ', y: ' + y);
		console.log('Cell     | ' + v);
	} else {
		let cell = v[0] + (WIDTH * v[1]);
		console.log('Position | x: ' + v[0] + ', y: ' + v[1]);
		console.log('Cell     | ' + cell);
	}

	rl.prompt();
}).on('close', function () {
	process.exit(0);
});
